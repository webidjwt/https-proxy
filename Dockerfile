# version 1.0 

FROM ubuntu:latest

MAINTAINER Jose G. Faisca <jose.faisca@gmail.com>

# -- Terminal variable --
ENV TERM xterm

# -- Install dependencies --
RUN apt-get update && apt-get install -y netcat curl socat

# -- Clean --
RUN cd / \
        && apt-get autoremove -y \
        && apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#  -- Ports for HTTPS --
EXPOSE 4433/tcp

COPY https-proxy.sh /usr/local/bin

CMD ["https-proxy.sh"]
