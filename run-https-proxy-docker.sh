#!/bin/bash
#
# Create docker container
#

if [ -z "$1" ]; then
 echo
 echo "Usage: ./$(basename $0) <container_name>"
 echo
 exit 1
fi

# Docker image
IMG="webidjwt/https-proxy"
TAG="latest"

# Docker network
NET_NAME="isolated_nw2"
JWTS_IP="10.18.0.9"

# Docker jwt-server container
JWTS_CT="$1"

# Run container
docker run -d \
	--net $NET_NAME --ip $JWTS_IP \
	--name $JWTS_CT \
	--restart=always \
	-p 4433:4433 \
	$IMG:$TAG




